// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
Cypress.Commands.add('SearchForItem', (product) => {

    cy.get('#searchInput').type(product).should('have.value', product)
    cy.log('Search For Product : ' + product)
    cy.get('ul > :nth-child(1) > .block').click()
    cy.log('Choose First Option')
})
Cypress.Commands.add('CheckOriginalPrice', (price) => {

    cy.get(':nth-child(1) > .ProductCard__link > .mt-auto > .line-through > :nth-child(1)')
        .should('have.text', price)
    cy.log('Check Product Price')
})
Cypress.Commands.add('CheckPriceAfterDiscount', (DiscountPrice) => {
    cy.get('div.h-full > .ProductCardGrid > :nth-child(1) > .ProductCard__link > .mt-auto > .text-primary-800 > .font-bold')
        .should('have.text', DiscountPrice)
    cy.log('Check Product Price After Discount')

})


Cypress.Commands.add('OpenProduct', (i) => {
    cy.xpath('(//*[contains(@class,"ProductCard_")]/picture)[' + i + ']').click({ force: true })


})
Cypress.Commands.add('AddItemToCard', () => {
    cy.wait(500)
    cy.get('[data-cy=add-to-card]').scrollIntoView()
    .should('be.visible').trigger("click");
    cy.wait(150)

   // cy.get('.btn-inverse').scrollIntoView()
    //.should('be.visible').trigger("click");

})

Cypress.Commands.add('GoBack', () => {
    cy.go('back')


})


Cypress.Commands.add('GoHome', () => {
    cy.get('.link > img').click()


})
Cypress.Commands.add('GetItemsCount', () => {
  
cy.get('//*/span[@class="absolute flex items-center justify-center right-0 top-0 rounded-full bg-yellow-100 text-xs h-sm w-sm"]').should(($div) => {
    const textCount = $div.text()
return textCount;
})



})




Cypress.Commands.add('CheckIFProductOutOFStock', () => {

    cy.get("body").then
        ($body => {

            if ($body.find(".self-start > .text-error").length > 0) {
              //  cy.GoBack()
            }
            else {
                cy.AddItemToCard()

            }


        })
})
Cypress.Commands.add('CheckProduct', () => {

    cy.get('[data-cy=add-to-card]') .then((x) => {
        if (x.is("enabled")) {
          //  cy.get('[data-cy=add-to-card]').click()
  //  cy.get('.btn-inverse').click({ force: true })
           cy.AddItemToCard()

    }
    else {
     //  cy.GoHome()


    }
   


})




})
