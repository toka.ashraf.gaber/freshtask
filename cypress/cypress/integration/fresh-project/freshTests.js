/// <reference types="cypress" />

Cypress.config('pageLoadTimeout', 100000)
Cypress.config('reporter', "cypress-mochawesome-reporter")

describe('Open Fresh Website', () => {
  let ProductData;

  beforeEach(() => {
    cy.log('Open WebSite')

    cy.visit(Cypress.env('url'))

  })


  beforeEach(() => {
    cy.log('Get Data')

    cy.fixture('fresh').then((data) => {
      ProductData = data;
      return ProductData;
    });
  })


  it('Search For Fresh Ice Box and Choose First Option', () => {


    cy.SearchForItem(ProductData.productName)
    cy.get('div.h-full > .ProductCardGrid > :nth-child(1)').scrollIntoView()
      .should('be.visible')
    cy.CheckOriginalPrice(ProductData.price)

    cy.CheckPriceAfterDiscount(ProductData.discountPrice)
cy.GoBack()
  //  for (var i = 1; i < 7; i++) {
      cy.log('Looping Products')
      cy.OpenProduct(4)
      cy.CheckProduct()
  


   // }



  })

})